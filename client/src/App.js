import React from 'react';
import Homepage from './components/homepage/homepage';
import Loginpage from './components/login/login';
import Registerpage from './components/register/register';
import './App.css'
import { BrowserRouter as Router,Routes, Route } from 'react-router-dom';

const App = () => {
    return (
       
            <Router>
                 <div className='App'>
                <Routes>
                    <Route path='/' exact element={<Homepage/>}/>
                    <Route path='/login' exact element={<Loginpage/>}/>
                    <Route path='/register' exact element={<Registerpage/>}/>
                </Routes>
                </div>
            </Router>

    );
}

export default App;