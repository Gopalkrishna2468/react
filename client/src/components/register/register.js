import React,{useState} from "react";
import './register.css'
import Axios from "axios";
import {useNavigate} from 'react-router-dom'


const Registerpage = () =>{

    const navigate = useNavigate()

    const [name1, setName] = useState(null);
    const [email1, setEmail] = useState(null);
    const [password1, setPassword] = useState(null);
    const [reEnterPassword1, setReEnterPassword] = useState(null);


    const postData = () =>{
        const Empdata = {
          name: name1,
          email: email1,
          password: password1,
          reEnterPassword: reEnterPassword1
        }

        if(Empdata.name && Empdata.email && Empdata.password && (Empdata.password === Empdata.reEnterPassword)){
            Axios.post('http://localhost:9000/register', Empdata).then((res) => window.location.reload())
        }else{
            alert('Invalid details')
        }
       
    }

    return (
        <div className='register'>
            <h1>Register</h1>
            <input type='text'     name='name' placeholder='Enter Name' value = {name1} onChange = {(e) =>{setName(e.target.value)}}></input>
            <input type='text'     name='email' placeholder='Enter Email' value = {email1} onChange = {(e) =>{setEmail(e.target.value)}}></input>
            <input type='password' name='password' placeholder='Enter Password' value = {password1} onChange = {(e) =>{setPassword(e.target.value)}}></input>
            <input type='password' name='reEnterPassword' placeholder='Re-Enter Password' value = {reEnterPassword1} onChange = {(e) =>{setReEnterPassword(e.target.value)}}></input>
            <div className='button' onClick = {() => postData()}>Register</div>
            <div>or</div>
            <div className='button'  onClick={() => navigate('/login')}>Login</div>
        </div>
    )
}

export default Registerpage;