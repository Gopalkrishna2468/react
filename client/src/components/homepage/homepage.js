import React from "react";
import './homepage.css'

const Homepage = () =>{
    return (
        <div className='homepage'>
            <p>Hello Homepage</p>
            <div className='button'>Logout</div>
        </div>
    )
}

export default Homepage;