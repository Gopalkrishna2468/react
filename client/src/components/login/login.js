import React,{useState} from "react";
import './login.css'
import Axios from "axios";
import {useNavigate} from 'react-router-dom'

const Loginpage = () =>{

    const navigate = useNavigate()

    const [email1, setEmail] = useState(null);
    const [password1, setPassword] = useState(null);



    const postData = () =>{
        const Empdata = {
            email:email1,
          password: password1,
        }
        if(Empdata.email && Empdata.password){
            Axios.post('http://localhost:9000/login', Empdata).then((res) => console.log(res.data.message))
        }else{
            alert('Invalid details')
        }
    }
    return (
        <div className='login'>
            <h1>Login</h1>
            <input type='text' placeholder='Enter your Email'  onChange = {(e) =>{setEmail(e.target.value)}}></input>
            <input type='password' placeholder='Enter your Password'  onChange = {(e) =>{setPassword(e.target.value)}}></input>
            <div className='button' onClick = {() => postData()}>Login</div>
            <div>or</div>
            <div className='button' onClick={() => navigate('/register')}>Register</div>
        </div>
    )
}

export default Loginpage;